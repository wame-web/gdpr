<?php

namespace Wame\Gdpr;

use Nette\DI\Container;
use Nette\Database\Context;
use Nette\Utils\Finder;
use Tracy\Debugger;
use Wame\Gdpr\Export\ExportRegister;
use Wame\Gdpr\Registers\GdprRegister;

class Export extends Base
{

    /** @var array */
    public $thirdParty;

    /** @var ExportRegister */
    private $exportRegister;

    /** {@inheritdoc} */
    public function __construct
    (
        Context $database,
        Container $container,
        GdprRegister $gdprRegister,
        ExportRegister $exportRegister
    )
    {
        parent::__construct($database, $container, $gdprRegister);
        $this->exportRegister = $exportRegister;
    }


    /** {@inheritdoc} */
    public function execute($token)
    {
        $request = $this->database->table(self::$databasePrefix . 'gdpr')
        ->where('token = ? AND status IS NULL', $token)
        ->fetch();


        if ($request->user_id == 0) {
            $key = array_search('users_id', $this->columns);
            unset($this->columns[$key]);
        }

        $this->getAllUserTables($this->columns);
        $items = $this->createService($request);
        $this->exportToFile($request, $items);
        $this->createZip($request->id);

        $this->database->table(self::$databasePrefix . 'gdpr')
          ->where(['user_id' => $request->user_id])
          ->update(['status' => 'completed']);

        return $request;

    }

    /** {@inheritdoc} */
    public function createService($request)
    {
        $output = $this->userCheckTables($request->user_id);

        $items = [];

        foreach ($this->gdprRegister->getList() as $item) {
            $items[$item['name']] = $item['service']
                ->create()
                ->setOutput($output)
                ->setUserId($request->user_id)
                ->setUserEmail($request->email);
        }

        return $items;

    }

    /**
     * Export to files e.g pdf, json
     *
     * @param object $request
     * @param array $items
     * @return void
     */
    private function exportToFile($request, $items)
    {
        foreach ($this->exportRegister->getList() as $export) {
            $export['service']->create()
            ->setItems($items)
            ->setRequestId($request->id)
            ->createFile();
        }
    }

    /**
     * Create zip from exported files
     *
     * @param integer $requestId
     * @return void
     */
    private function createZip($requestId)
    {
        $zip = new \ZipArchive();
        $dir = ABSOLUTE_PATH . '/../files/gdpr/' . $requestId;

        foreach (Finder::findFiles('*')->from($dir) as $file) {
            $zip->open($dir . '/' . $requestId .'.zip', \ZipArchive::CREATE);
            $zip->addFile($file->getPathName(), $file->getFilename());
            $zip->close();
            unlink($file->getPathName());
        }

    }

    /**
     * Set third party parameter
     *
     * @param array $thirdParty
     * @return object Export
     */
    public function setThirdParty($thirdParty)
    {
        $this->thirdParty = $thirdParty;
        return $this;
    }

    /**
     * Get third party parameter
     *
     * @return array of thirdParty
     */
    public function getThirdParty()
    {
        return $this->thirdParty;
    }

}
