<?php
namespace Wame\Gdpr;

use Nette\Database\Context;
use Nette\DI\Container;
use Wame\Gdpr\Registers\GdprRegister;

abstract class Base
{
    /** @var array $columns */
    protected $columns;

    /** @var object Context */
    protected $database;

    /** @var array $requestedUsers */
    private $requestedUsers;

    /** @var array $tables */
    protected $tables = [];

    /** @var array $output */
    protected $output = [];

    /** @var object Container */
    protected $container;

    /** @var string $databaseName */
    protected $databaseName;

    /** @var object GdprRegister */
    protected $gdprRegister;

    /** @var string $databasePrefix */
    public static $databasePrefix;

    /**
     * @param Context - Context is used for work with database
     * @param Container - Container used for getting services
     * @param GdprRegister - To get all types
     */
    public function __construct(
        Context $database,
        Container $container,
        GdprRegister $gdprRegister
    )
    {
        $this->database = $database;
        $this->container = $container;
        $this->gdprRegister = $gdprRegister;

        $this->databaseName = $container->getParameters()['database']['dbname'];
        self::$databasePrefix = $container->getParameters()['database']['prefix'];
    }

    /**
     * Execute process
     *
     * @param string $token
     * @return void
     */
    abstract public function execute($token);

    /**
    * Create service from types
    *
    * @param integer $requestedUserId
    * @return mixed
    */
    abstract public function createService($requestedUserId);

    /**
     * Get all user tables by columns
     *
     * @param array $columns
     * @return Export
     */
    protected function getAllUserTables($columns)
    {
        foreach ($columns as $column) {
            $tables = $this->database->query("SELECT DISTINCT TABLE_NAME "
                . "FROM INFORMATION_SCHEMA.COLUMNS "
                . "WHERE COLUMN_NAME = ? "
                . "AND TABLE_SCHEMA = ? ", $column, $this->databaseName)->fetchAll();

            foreach ($tables as $table) {
                $this->tables[$table->TABLE_NAME][] = $column;
            }
        }

        return $this;
    }

    /**
     * Check if user id is in tables
     *
     * @param integer $userId
     * @return array $output
     */
    protected function userCheckTables($userId)
    {
        $output = [];

        foreach ($this->tables as $table => $columns) {
            $args = [];
            $query = "SELECT * FROM " . $table . " WHERE ";

            foreach ($columns as $column) {
                $query .= $column . ' = ? OR ';
                $args[] = $userId;
            }

            $query = substr($query, 0, -4);

            $data = $this->database->queryArgs($query, $args)->fetchAll();

            if ($data) {
                $output[$table] = $data;
            }
        }

        return $output;
    }

    /**
     * Setter for columns
     *
     * @param array $columns
     * @return Export
     */
    public function setColumns(array $columns)
    {
        $this->columns = $columns;
        return $this;
    }

    /**
     * Getter for columns
     *
     * @return Columns
     */
    public function getColumns()
    {
        return $this->columns;
    }
}
