<?php

namespace Wame\Gdpr\Registers;

use Nette\InvalidArgumentException;
use Nette\Object;


abstract class BaseRegister
{


    /** @var string Type */
    protected $type;

    /** @var array Services list */
    protected $list = [];

    /** @var array Default parameters list */
    protected $defaultParameters = [];


    /**
     * @param string $type Name of class accepted in this register
     */
    public function __construct($type)
    {
        $this->type = $type;
    }


    /**
     * Set default parameters
     *
     * @param string $key
     * @param mixed $value
     *
     * @return $this
     */
    public function addDefaultParameter($key, $value)
    {
        $this->defaultParameters[$key] = $value;

        return $this;
    }


    /**
     * Register service into register
     *
     * @param Object $service Service object
     * @param string|null $name Service key name
     * @param array $parameters Service parameters
     *
     * @return $this
     */
    public function add($service, $name = null, $parameters = [])
    {
        if (!$service) {
            throw new InvalidArgumentException(sprintf('Trying to insert invalid service %s to %s register.', $name, get_class($this)));
        }

        if (!is_array($parameters)) {
            throw new InvalidArgumentException(sprintf('Third parameter in register %s has to by array `%s` given.', get_class($this), $parameters));
        }

        if (is_a($service, $this->type)) {
            if (!$name) {
                $name = ObjectHelper::getNamespace($service);
            }

            $this->list[$name] = [
                'service' => $service,
                'parameters' => array_merge($this->defaultParameters, $parameters)
            ];
        } else {
            throw new InvalidArgumentException(sprintf('Trying to register class %s into register of %s.', get_class($service), $this->type));
        }

        return $this;
    }


    /**
     * Remove service from register
     *
     * @param string|Object $name Key name or object
     *
     * @return $this
     */
    public function remove($name)
    {
        if (is_object($name)) {
            $key = $this->getKeyByService($name);

            if ($key) unset($this->list[$key]);
        } else {
            unset($this->list[$name]);
        }

        return $this;
    }


    /**
     * Check exists service
     *
     * @param string|Object $name Key name or object
     *
     * @return bool
     */
    public function isExists($name)
    {
        $item = $this->getByName($name);

        return $item !== null ? true : false;
    }


    /**
     * Get all registered services
     *
     * @return array
     */
    public function getList()
    {
        return $this->list;
    }


    /**
     * Get service by name
     *
     * @param string|Object $name Key name or object
     *
     * @return Object|null Service
     */
    public function getByName($name)
    {
        if (is_object($name)) {
            $key = $this->getKeyByService($name);

            if ($key) return $this->list[$key];
        } else {
            return $this->list[$name];
        }

        return null;
    }


    /**
     * Get list key by service
     *
     * @param Object $service
     *
     * @return int|null|string
     */
    public function getKeyByService($service)
    {
        foreach ($this->getList() as $key => $values) {
            if (self::getNamespace($values['service']) == self::getNamespace($service)) {
                return $key;
            }
        }

        return null;
    }


    /**
     * Get service object by name
     *
     * @param string|Object $name Key name or object
     *
     * @return Object|null
     */
    public function getServiceByName($name)
    {
        $item = $this->getByName($name);

        if ($item) {
            return $item['service'];
        }

        return null;
    }


    /**
     * Get service parameters by name
     *
     * @param string|Object $name Key name or object
     *
     * @return array
     */
    public function getParametersByName($name)
    {
        $item = $this->getByName($name);

        if ($item) {
            return $item['parameters'];
        }

        return [];
    }


    /**
     * Get service parameter by key
     *
     * @param string|Object $name Key name or object
     * @param string $key Service parameter key
     *
     * @return mixed
     */
    public function getServiceParameter($name, $key)
    {
        $parameters = $this->getParametersByName($name);

        if (isset($parameters[$key])) {
            return $parameters[$key];
        }

        return null;
    }


    /**
     * Returns an iterator over all items
     *
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator((array) $this->getList());
    }

    /**
     * Get class namespace
     *
     * @param Object $object
     *
     * @return string
     */
    public static function getNamespace($object)
    {
        return get_class($object);
    }

}