<?php

namespace Wame\Gdpr\Registers\Types;

use Nette\Application\UI\ITemplateFactory;
use Nette\Database\Context;
use Nette\DI\Container;
use Wame\Gdpr\Export;

interface ThirdPartyControlFactory extends BaseTypeControlFactory
{
    /** @return ThirdPartyControl */
    public function create();
}

class ThirdPartyControl extends BaseTypeControl
{

    /** @var Export */
    public $export;

    /** {@inheritdoc} */
    public function __construct
    (
      ITemplateFactory $ITemplateFactory,
      Context $context,
      Export $export
    )
    {
        parent::__construct($ITemplateFactory, $context);
        $this->export = $export;
    }

    /** {@inheritdoc} */
    public function getTitle()
    {
        return 'thirdParty';
    }

    /** {@inheritdoc} */
    protected function getTemplatePath()
    {
        return __DIR__ . '/default.latte';
    }

    /**
     * @return array of user
     */
    public function prepareDataForRender()
    {
        return [
            'thirdParties' => $this->getData()
        ];
    }

    /** {@inheritdoc} */
    public function getData()
    {
        return $this->export->getThirdParty();
    }

    /** {@inheritdoc} */
    public function delete()
    {
        // TODO: Implement delete() method.
    }


}
