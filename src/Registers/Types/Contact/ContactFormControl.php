<?php

namespace Wame\Gdpr\Registers\Types;

use Nette\Application\UI\ITemplateFactory;
use Nette\Database\Context;

interface ContactFormControlFactory extends BaseTypeControlFactory
{
    /** @return ContactFormControl */
    public function create();
}

class ContactFormControl extends BaseTypeControl
{
    /** {@inheritdoc} */
    public function __construct
    (
      ITemplateFactory $ITemplateFactory,
      Context $context
    )
    {
        parent::__construct($ITemplateFactory, $context);
    }

    /** {@inheritdoc} */
    public function getTitle()
    {
        return 'ContactFormControl';
    }

    /** {@inheritdoc} */
    protected function getTemplatePath()
    {
        return __DIR__ . '/default.latte';
    }

    /**
     * @return array of user
     */
    public function prepareDataForRender()
    {
        return [
            'data' => $this->getData()
        ];
    }

    /** {@inheritdoc} */
    public function getData()
    {
        $data = [];

        $users = $this->database
            ->table($this->prefix . 'contact_forms')
            ->where(['email' => $this->getUserEmail()])
            ->fetchAll();

        foreach ($users as $user) {
            $data[] = $this->mergeUsersData($user);
        }

        return $data;

    }

    private function mergeUsersData($select)
    {
        $data = [];

        foreach ($select as $key => $value) {
            if (!$value) continue;
            if (in_array($key, $this->getBlacklist())) continue;
            $data[$key] = $value;
        }
        return $data;
    }

    /**
     * @return array of user tables
     */
    private function getBlackList()
    {
        return [
        ];
    }

    /** {@inheritdoc} */
    public function delete()
    {
        $this->database->table($this->prefix . 'contact_forms')
            ->where(['email' => $this->getUserEmail()])
            ->update(['email' => self::anonymize($this->getUserEmail())]);
    }

}
