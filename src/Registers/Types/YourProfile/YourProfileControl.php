<?php

namespace Wame\Gdpr\Registers\Types;

use Composer\Command\DumpAutoloadCommand;
use Nette\Application\UI\ITemplateFactory;
use Nette\Database\Context;
use Nette\Utils\Strings;
use Tracy\Debugger;

interface YourProfileControlFactory extends BaseTypeControlFactory
{
    /** @return YourProfileControl */
    public function create();
}

class YourProfileControl extends BaseTypeControl
{
    /** {@inheritdoc} */
    public function __construct(ITemplateFactory $ITemplateFactory, Context $context)
    {
        parent::__construct($ITemplateFactory, $context);
    }

    /** {@inheritdoc} */
    public function getTitle()
    {
        return 'yourProfile';
    }

    /** {@inheritdoc} */
    protected function getTemplatePath()
    {
        return __DIR__ . '/default.latte';
    }

    /**
     * @return array of user
     */
    public function prepareDataForRender()
    {

        return [
            'user' => $this->getData()
        ];
    }

    /** {@inheritdoc} */
    public function getData()
    {
        $user = $this->database->table($this->prefix . 'users')
            ->where(['users_info_id' => $this->getUserId()])
            ->limit(1)
            ->fetch();

        $usersInfo = $this->database->table($this->prefix . 'users_info')
            ->where(['id' => $this->getUserId()])
            ->limit(1)
            ->fetch();

        if (!$user || !$usersInfo) {
             $nonRegisteredUser = $this->getNonRegisterUserInfo($this->getUserEmail());
             return $this->mergeUsersData($nonRegisteredUser);
        }

        $userMerge = $this->mergeUsersData($user);
        $userInfoMerge = $this->mergeUsersData($usersInfo);

        $data = array_merge($userMerge, $userInfoMerge);

        return $data;

    }

    /**
    * @param array $select
    *
    * @return array $data - return user info from more tables
    */
    private function mergeUsersData($select)
    {
        $data = [];

        foreach ($select as $key => $value) {
            if (!$value) continue;
            if (in_array($key, $this->getBlacklist())) continue;
            $data[$key] = $value;
        }
        return $data;
    }

    /** {@inheritdoc} */
    public function delete()
    {
        $user = $this->database->table($this->prefix . 'users')
            ->where(['users_info_id' => $this->getUserId()])
            ->limit(1)
            ->fetch();

        if ($user) {
            $this->deleteRegisteredUser($user);
        }

        $shopOrdersUserInfo = $this->database->table($this->prefix . 'shop_orders_user_info')
            ->where(['email' => $this->getUserEmail()])
            ->fetchAll();

        $data = [];

        foreach ($shopOrdersUserInfo as $key => $row) {

            foreach ($row as $column => $value) {
                if (!$value) continue;
                if (in_array($column, $this->getBlacklist())) continue;
                $data[$column] = self::anonymize($value);
            }

        }

        $shopOrdersUserInfo = $this->database->table($this->prefix . 'shop_orders_user_info')
            ->where(['email' => $this->getUserEmail()])
            ->update($data);

    }

    private function deleteRegisteredUser($user)
    {
        $userInfo = $this->database->table($this->prefix . 'users_info')
            ->where(['id' => $user->users_info_id])
            ->limit(1)
            ->fetch();

        $anonymizationUserInfo = [
            'realFirstName' => self::anonymize($userInfo['realFirstName']),
            'realLastName' => self::anonymize($userInfo['realLastName']),
            'phone' => self::anonymize($userInfo['phone']),
            'street' => self::anonymize($userInfo['street']),
            'zipCode' => self::anonymize($userInfo['zipCode']),
            'city' => self::anonymize($userInfo['city']),
            'state' => self::anonymize($userInfo['state']),
            'houseNumber' => self::anonymize($userInfo['houseNumber']),
            'shipping_street' => self::anonymize($userInfo['shipping_street']),
            'shipping_houseNumber' => self::anonymize($userInfo['shipping_houseNumber']),
            'shipping_zipCode' => self::anonymize($userInfo['shipping_zipCode']),
            'shipping_city' => self::anonymize($userInfo['shipping_city']),
            'shipping_state' => self::anonymize($userInfo['shipping_state']),
            'shipping_realFirstName' => self::anonymize($userInfo['shipping_realFirstName']),
            'shipping_realLastName' => self::anonymize($userInfo['shipping_realLastName']),
            'shipping_phone' => self::anonymize($userInfo['shipping_phone'])
        ];

        $anonymizationUser = [
            'email' => self::anonymize($user['email']),
            'real_name' => self::anonymize($user['real_name']),

        ];

        $user_table = $this->database
            ->table($this->prefix . 'users')
            ->where(['users_info_id' => $user->users_info_id])
            ->update($anonymizationUser);

        $user_info_table = $this->database
            ->table($this->prefix . 'users_info')
            ->where(['id' => $user->users_info_id])
            ->update($anonymizationUserInfo);

        $anonymizationUserInfo += ['email' => self::anonymize($this->getUserEmail())];

        $this->database
            ->table($this->prefix . 'shop_orders_user_info')
            ->where(['email' => $this->getUserEmail()])
            ->update($anonymizationUserInfo);
    }

    /**
     * @return array of user tables
     */
    private function getBlackList()
    {
        return [
            'id', 'import_user_id', 'sameAsShipping', 'import_email',
            'user_info_id', 'parenter_project_id', 'customer_id', 'password',
            'role', 'prices_group', 'real_name', 'rating',
            'welcome_page', 'mailing_status', 'status'
        ];
    }

    private function getNonRegisterUserInfo($email)
    {
        return $this->database->table($this->prefix . 'shop_orders_user_info')
            ->where(['email' => $email])->fetch();
    }


}
