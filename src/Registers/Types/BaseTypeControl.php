<?php
namespace Wame\Gdpr\Registers\Types;

use Nette\Application\UI\Control;
use Nette\Application\UI\ITemplateFactory;
use Nette\ComponentModel\IContainer;
use Nette\Database\Context;
use Nette\Utils\Strings;
use Tracy\Debugger;
use Wame\Gdpr\Export;


interface BaseTypeControlFactory
{
    /** @return BaseTypeControl */
    public function create();
}


abstract class BaseTypeControl extends Control
{
    /** @var ITemplateFactory */
    protected $templateFactory;

    /** @var object Context */
    protected $database;

    /** @var array $output*/
    private $output;

    /** @var integer $userId*/
    private $userId;

    /** @var string $userEmail */
    private $userEmail;

    /** @var string $prefix */
    protected $prefix;

    /**
     * @param ITemplateFactory Factory used for creating template in component
     * @param object $context
     */
    public function __construct(ITemplateFactory $ITemplateFactory, Context $context)
    {
        $this->templateFactory = $ITemplateFactory;
        $this->database = $context;
        $this->prefix = Export::$databasePrefix;
    }

    /**
     * @param array $output
     * @return BaseTypeControl
     */
    public function setOutput($output)
    {
        $this->output = $output;
        return $this;
    }

    /**
     * @param string $name
     * @return array output
     */
    public function getOutput($name = null)
    {
        if ($name) {
            if (!isset($this->output[$name])) return null;

            return $this->output[$name];
        }

        return $this->output;
    }

    /**
     * @param integer $userId
     * @return BaseTypeControl
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return $userId
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param integer $userId
     * @return BaseTypeControl
     */
    public function setUserEmail($email)
    {
        $this->userEmail = $email;
        return $this;
    }

    /**
     * @return $userId
     */
    public function getUserEmail()
    {
        return $this->userEmail;
    }

    /**
     * @return directory of template
     */
    protected abstract function getTemplatePath();

    /**
     * @return array of data
     */
    public abstract function getData();

    /**
     * @return string of title
     */
    public abstract function getTitle();

    /**
     * @return string of title
     */
    public abstract function delete();


    /**
     * Render component
     */
    public function render()
    {
        $template = $this->templateFactory->createTemplate();

        foreach ($this->prepareDataForRender() as $key => $value) {
            $template->$key = $value;
        }

        $template->setFile($this->getTemplatePath());
        $template->render();
    }

    /**
    * Method for anonymize string
    *
    * @param string $string
    *
    * @return string
    */
    public static function anonymize($string)
    {
        $return = '';

        foreach (explode(' ', Strings::toAscii($string)) as $word) {
            if (strlen($word) > 2) {
                $return .= substr($word, 0, 1) . str_repeat('*', strlen($word) - 2) . substr($word, -1) . ' ';

            } else {
                $return .= '**';
            }
        }

        return substr($return, 0, -1);
    }
}
