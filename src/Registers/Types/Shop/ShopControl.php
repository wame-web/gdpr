<?php
namespace Wame\Gdpr\Registers\Types;

use Nette\Application\UI\ITemplateFactory;
use Nette\ArrayHash;
use Nette\Database\Context;
use Tracy\Debugger;

interface ShopControlFactory extends BaseTypeControlFactory
{
    /** @return ShopControl */
    public function create();
}

class ShopControl extends BaseTypeControl
{

    /** {@inheritdoc} */
    public function __construct(ITemplateFactory $ITemplateFactory, Context $context)
    {
        parent::__construct($ITemplateFactory, $context);
    }

    /** {@inheritdoc}*/
    public function getTitle()
    {
        return 'shopOrders';
    }

    /** {@inheritdoc}*/
    protected function getTemplatePath()
    {
        return __DIR__ . '/default.latte';
    }

    /**
     * @return array of shop data
     */
    public function prepareDataForRender()
    {

        $userInfo = $this->database->table('shop_orders_user_info')
                ->where(['email' => $this->getUserEmail()])
                ->fetchAll();
        $orders = [];

        foreach ($userInfo as $value) {
            $dbOrder = $this->database->table('shop_orders')
                ->where(['id' => $value->shop_orders_id])
                ->fetch();

            $orders[] = $dbOrder;
        }

        if (empty($orders)) return [
            'orders' => [],
            'items' => [],
            'invoices' => []
        ];

        $orderIds = $this->getOrderIds($orders);
        $orderItems = $this->getOrderItems($orderIds);
        $orderProducts = $this->getOrderProducts($orderItems);
//        $orderInvoices = $this->getInvoices($orderIds);

        return [
            'orders' => $orders,
            'items' => $orderItems,
            'products' => $orderProducts
//            'invoices' => $orderInvoices
        ];
    }

    /**
     * @param array $orderIds
     *
     * @return array of orders
     */
    private function getOrderItems($orderIds)
    {
        $items = $this->database->table($this->prefix . 'shop_orders_items')
            ->where(['shop_orders_id IN (?)' => $orderIds, 'status' => 1]);

        $return = [];

        foreach ($items as $item) {

            $return[$item->shop_orders_id][] = $item;
        }

        return $return;
    }

    private function getOrderProducts($orderItems)
    {
        $return = [];
        foreach ($orderItems as $item) {

            foreach ($item as $value) {
                $product = $this->database->table('shop_products_sk')
                    ->where(['id' => $value->product_id])->limit(1)->fetch();

                $return[$value->id] = $product;
            }
        }

        return $return;
    }

    /**
     * @param array $invoices
     *
     * @return array of invoices
     */
    private function getInvoices($orderIds)
    {
        $items = $this->database
            ->table($this->prefix . 'shop_order_invoice')
            ->where(['shop_order_id IN (?)' => $orderIds, 'status' => 1]);

        $return = [];

        foreach ($items as $item) {
            $return[$item->shop_order_id][] = $item;
        }

        return $return;
    }

    /**
     * @param array $orders
     *
     * @return array of order ids
     */
    private function getOrderIds($orders)
    {
        $return = [];
        foreach ($orders as $order) {
            $return[] = $order->id;
        }

        return $return;
    }

    /** {@inheritdoc} */
    public function getData()
    {
        return $this->prepareDataForRender();
    }

    /** {@inheritdoc} */
    public function delete()
    {

        if ($this->getUserId() != null || $this->getUserId() != 0) {
            $this->deleteRegisteredUser();
        } else {
            $this->deleteNonRegisteredUser();
        }
    }

    private function deleteRegisteredUser()
    {
        $shopOrder = $this->database->table($this->prefix . 'shop_orders')
            ->where(['users_id' => $this->getUserId()])->fetch();

        if ($shopOrder) {
            $this->database->table($this->prefix . 'shop_orders')
                ->where(['users_id' => $this->getUserId()])
                ->update(['shopper_full_name' => self::anonymize($shopOrder->shopper_full_name)]);
        }
    }

    private function deleteNonRegisteredUser()
    {
        $shopOrderUserInfo = $this->database->table($this->prefix . 'shop_orders_user_info')
            ->where(['email' => $this->getUserEmail()])
            ->fetchAll();

        foreach ($shopOrderUserInfo as $item) {
            $shopOrder = $this->database->table($this->prefix . 'shop_orders')
                ->where(['id' => $item->shop_orders_id])->fetch();

            $this->database->table($this->prefix . 'shop_orders')
                ->where(['id' => $item->shop_orders_id])
                ->update(['shopper_full_name' => self::anonymize($shopOrder->shopper_full_name)]);
        }
    }
}
