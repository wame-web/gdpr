<?php

namespace Wame\Gdpr\Registers\Types;

use Nette\Application\UI\ITemplateFactory;
use Nette\Database\Context;
use Tracy\Debugger;

interface GdprControlFactory extends BaseTypeControlFactory
{
    /** @return GdprControl */
    public function create();
}

class GdprControl extends BaseTypeControl
{
    /** {@inheritdoc} */
    public function __construct
    (
      ITemplateFactory $ITemplateFactory,
      Context $context
    )
    {
        parent::__construct($ITemplateFactory, $context);
    }

    /** {@inheritdoc} */
    public function getTitle()
    {
        return 'GdprControl';
    }

    /** {@inheritdoc} */
    protected function getTemplatePath()
    {
        return __DIR__ . '/default.latte';
    }

    /**
     * @return array of user
     */
    public function prepareDataForRender()
    {
        return [
            'gdpr' => $this->getData()
        ];
    }

    /** {@inheritdoc} */
    public function getData()
    {
        $data = $this->database
            ->table($this->prefix . 'gdpr')
            ->where(['email' => $this->getUserEmail()])
            ->fetchAll();

        return $data;

    }

    /** {@inheritdoc} */
    public function delete()
    {
//        $delete = $this->database->table($this->prefix . 'gdpr')
//            ->where(['user_id' => $this->getUserId()])
//            ->delete();
//
//        Debugger::log('User with ID'. $this->getUserId() . 'in GDPR table deleted');

        $this->database->table($this->prefix . 'gdpr')
            ->where(['email' => $this->getUserEmail()])
            ->update(['user_id' => 0,'email' => self::anonymize($this->getUserEmail())]);


    }

}
