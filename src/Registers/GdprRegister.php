<?php
namespace Wame\Gdpr\Registers;

use Wame\Gdpr\Registers\Types\BaseTypeControlFactory;

class GdprRegister extends PriorityRegister
{
    public function __construct()
    {
        parent::__construct(BaseTypeControlFactory::class);
    }

}