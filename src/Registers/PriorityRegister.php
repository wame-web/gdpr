<?php

namespace Wame\Gdpr\Registers;

use Nette\InvalidArgumentException;


class PriorityRegister extends BaseRegister
{
    public function __construct($type)
    {
        parent::__construct($type);

        $this->addDefaultParameter('priority', 0);
    }


    /** {@inheritdoc} */
    public function add($service, $name = null, $parameters = [])
    {
        if (!$service) {
            throw new InvalidArgumentException(sprintf('Trying to insert invalid service %s to %s register.', $name, get_class($this)));
        }

        if (!is_array($parameters)) {
            throw new InvalidArgumentException(sprintf('Third parameter in register %s has to by array `%s` given.', get_class($this), $parameters));
        }

        $parameters = array_replace($this->defaultParameters, $parameters);

        if (is_a($service, $this->type)) {
            if (!$name) {
                $name = self::getNamespace($service);
            }

            $index = $this->getIndexByName($name);

            if ($index >= 0) {
                $this->list[$index]['service'] = $service;
                $this->list[$index]['parameters'] = array_merge($this->list[$index]['parameters'], $parameters);
            } else {
                $this->list[] = [
                    'name' => $name,
                    'service' => $service,
                    'parameters' => $parameters
                ];
            }

            $this->resort();
        } else {
            throw new InvalidArgumentException(sprintf('Trying to register class %s into register of %s.', get_class($service), $this->type));
        }

        return $this;
    }


    /** {@inheritdoc} */
    public function remove($name)
    {
        $index = $this->getIndexByName($name);

        if ($index >= 0) {
            unset($this->list[$index]);
        }

        $this->resort();

        return $this;
    }


    /** {@inheritdoc} */
    public function isExists($name)
    {
        return $this->getIndexByName($name) >= 0 ? true : false;
    }


    /** {@inheritdoc} */
    public function getByName($name)
    {
        $index = $this->getIndexByName($name);

        if ($index >= 0) {
            return $this->list[$index];
        }

        return null;
    }


    /**
     * Get index by name
     *
     * @param string|Object $name name or service
     *
     * @return int Index of service or -1 if not found
     */
    public function getIndexByName($name)
    {
        for ($i = 0; $i < count($this->getList()); $i++) {
            if (is_object($name)) {
                if ($this->list[$i]['service'] == $name) return $i;
            } else {
                if ($this->list[$i]['name'] == $name) return $i;
            }
        }

        return -1;
    }


    /**
     * Returns an iterator over all items.
     *
     * @return \RecursiveArrayIterator
     */
    public function getIterator()
    {
        return new \RecursiveArrayIterator((array) $this->getList());
    }


    /**
     * Resort services by priority
     */
    private function resort()
    {
//        usort($this->getList(), function ($s1, $s2) {
//            return $s2['parameters']['priority'] - $s1['parameters']['priority'];
//        });
    }

}