<?php
namespace App\Presenters;


use Nette;
use App\Presenters\BasePresenter;
use Nette\Application\Responses\FileResponse;
use Tracy\Debugger;
use Wame\Gdpr\Delete;
use Wame\Gdpr\Export;
use Nette\Utils\Random;
use Nette\Mail\Message;
use Wame\Gdpr\Registers\GdprRegister;
use Nette\Application\UI;

class GdprPresenter extends BasePresenter
{
    /** @var Export @inject */
    public $export;

    /** @var Delete @inject */
    public $delete;

    /** @var $database */
    public $database;

    /** @var Nette\Application\LinkGenerator */
    private $linkGenerator;

    /** @var Nette\Application\UI\ITemplateFactory */
    private $templateFactory;

    /** @var GdprRegister @inject */
    public $gdprRegister;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }


    /** Action to execute export */
    public function actionExport($token)
    {
        $request = $this->database->table('gdpr')
            ->where('token = ? AND status IS NULL', $token)
            ->fetch();

        if (!$request) {
            $this->presenter->flashMessage('Váš token je neplatný, skúste požiadať znova');
            header('http://dukra.sk');
            exit();
        }

        $this->template->setFile(__DIR__ . '/default.latte');
        $request = $this->export->execute($token);
//        $requestedUser = $this->database->table('wame_gdpr')->where(['token' => $token, 'download_date' => NULL])->fetch();

        $file = ABSOLUTE_PATH . '/../files/gdpr/' . $request->id . '/' .$request->id . '.zip';

        $this->template->setFile(__DIR__ . '/download.latte');
        $this->database->table('gdpr')->where(['token' => $token])->update(['download_date' => date("Y-m-d")]);

        $this->template->presenter->sendResponse(new FileResponse($file, '', 'application/download'));
    }

    /** Action to execute delete */
    public function actionDelete($token)
    {
        $this->template->setFile(__DIR__ . '/delete.latte');
        $this->delete->execute($token);
        header("Location: http://dukra.sk");
    }



}