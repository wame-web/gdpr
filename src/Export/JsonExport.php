<?php
namespace Wame\Gdpr\Export;

interface JsonExportFactory extends BaseExportFactory
{
    /** @return JsonExport */
    public function create();
}
class JsonExport extends BaseExport
{
    public function createFile()
    {
        $items = [];
        foreach ($this->items as $name => $item) {
            $items[$name] = $item->getData();
        }
       $json = json_encode($items);
       $file = $this->getFolder($this->requestId) . '/' . date("Y-m-d") . '-' . $this->requestId . '.json';
       file_put_contents($file, $json);
    }
}