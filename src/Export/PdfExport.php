<?php

namespace Wame\Gdpr\Export;

use Nette\Application\UI\ITemplateFactory;

interface PdfExportFactory extends BaseExportFactory
{
    /** @return PdfExport */
    public function create();
}
class PdfExport extends BaseExport
{
    /** @var ITemplateFactory */
    private $templateFactory;

    public function __construct(ITemplateFactory $ITemplateFactory)
    {
        $this->templateFactory = $ITemplateFactory;
    }

    public function createFile()
    {
        $template = $this->templateFactory->createTemplate();
        $template->items = $this->items;
        $template->setFile(__DIR__ . '/pdf.latte');

        $pdf = new \Joseki\Application\Responses\PdfResponse($template);
        // optional
        $pdf->documentTitle = date("Y-m-d") . '-' .$this->requestId; // creates filename 2012-06-30-my-super-title.pdf
        $pdf->pageFormat = "A4"; // wide format
        $pdf->getMPDF()->setFooter("Wame s.r.o."); // footer
        $pdf->save($this->getFolder($this->requestId)); // as a filename $this->documentTitle will be used

    }
}