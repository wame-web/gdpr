<?php
namespace Wame\Gdpr\Export;

use Wame\Gdpr\Export\BaseExportFactory;
use Wame\Gdpr\Registers\BaseRegister;

class ExportRegister extends BaseRegister
{
    public function __construct()
    {
        parent::__construct(BaseExportFactory::class);
    }

}