<?php
namespace Wame\Gdpr\Export;

interface BaseExportFactory
{
    /** @return BaseExport */
    public function create();
}

abstract class BaseExport
{
    /** @var array $items */
    protected $items;

    /** @var integer $requestId */
    protected $requestId;

    public function setItems($items)
    {
        $this->items = $items;
        return $this;
    }

    public function setRequestId($id)
    {
        $this->requestId = $id;
        return $this;
    }

    public abstract function createFile();

    protected function getFolder($requestId)
    {
        $dir = ABSOLUTE_PATH . '/../files/gdpr/' . $requestId;
        $path = str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $dir);

        if (!file_exists($path)) {
            mkdir($path, 0755, true);
        }

        return $path;
    }
}