<?php
namespace Wame\Gdpr;

class Delete extends Base
{
    /** {@inheritdoc} */
    public function execute($token)
    {
        $request = $this->database
          ->table(self::$databasePrefix . 'gdpr')
          ->where('token = ? AND status IS NULL', $token)
          ->fetch();

        if (!$request) {
            header('Location: http://dukra.sk/');
            exit();
        }

        $this->getAllUserTables($this->columns);
        $items = $this->createService($request);

        $this->database->table(self::$databasePrefix . 'gdpr')
            ->where(['user_id' => $request->user_id])
            ->update(['status' => 'deleted']);

    }

    /** {@inheritdoc} */
    public function createService($request)
    {
        $output = $this->userCheckTables($request->user_id);

        foreach ($this->gdprRegister->getList() as $item) {
            $items[$item['name']] = $item['service']
                ->create()
                ->setOutput($output)
                ->setUserId($request->user_id)
                ->setUserEmail($request->email)
                ->delete();
        }

    }
}
