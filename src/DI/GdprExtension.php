<?php

namespace Wame\Gdpr\DI;

use Nette\DI\CompilerExtension;
use Wame\Gdpr\Delete;
use Wame\Gdpr\Export;

class GdprExtension extends CompilerExtension
{
    public $defaults = [
        'columns' => [
            'user_id', 'create_user_id', 'edit_user_id'
        ],
        'thirdParty' => []
    ];

    public function loadConfiguration()
    {
        $this->config += $this->defaults;

        $builder = $this->getContainerBuilder();
        $this->compiler->parseServices($builder, $this->loadFromFile(__DIR__ . '/../Config/config.neon'), $this->name);


//        $builder->addDefinition($this->prefix('Gdpr'))
//            ->setClass(Export::class)
//            ->addSetup('setColumns', [$this->config['columns']])
//            ->addSetup('setThirdParty', [$this->config['thirdParty']]);

    }

    public function beforeCompile()
    {
        parent::beforeCompile();

        $builder = $this->getContainerBuilder();

        $builder->getDefinition($builder->getByType(Export::class))->addSetup('setColumns', [$this->config['columns']])->addSetup('setThirdParty', [$this->config['thirdParty']]);
        $builder->getDefinition($builder->getByType(Delete::class))->addSetup('setColumns', [$this->config['columns']]);

    }
}