# GDPR module

## Flow modulu (babyceremony.localhost) priklad
1. Zakaznik pride na url babyceremony.localhost/gdpr/request
2. Zada svoj email. Na jeho email mu pride odkaz s vygenerovanym tokenom kde ked sa preklikne dostane sa na 3.
3. babyceremony.localhost/gdpr/landingPage?token=afadsfs ... na tejto podstranke ma na vyber z moznosti kde moze poziadat o odstranenie alebo o vyexportovanie jeho dat

## Instalacia modulu
1. Naklonovat projekt do ktoreho sa bude implementovat GDPR modul
2. Pridat modul do composera, potom composer update
3. Prekopirovat z priecinka db subory pre migraciu do projektoveho priecinka db ktory sa nachadza v app. Potom spustit migraciu a tabulka by mala byt pripravena.
4. Ak je modul uspesne nainstalovany, je potrebne pridat extension do hlavneho configu ktory sa nachadza v priecinku app
5. Je potrebne pozriet databazu daneho projektu a zistit ake stlpce sa tam nachadzaju ktore obsahuju user identifikator. Defaultne su nastavene user_id, create_user_id, edit_user_id. Je moznost ich pretazovat v configu projektu kde zadame columns.


# Registrovanie extension
```
extensions:
    gdpr: Wame\Gdpr\DI\GdprExtension
```
# Priklad k pretazovaniu stlpcov a registrovanie tretej strany.

```
gdpr:
    thirdParty: {
            GoogleAnalytics: '<p>Bližšie informácie o&nbsp;ochrane súkromia <a class="link" href="https://support.google.com/analytics/topic/2919631?hl=sk&amp;ref_topic=1008008" target="_blank">nájdete tu</a>, pričom na odmietnutie cookies si môžete inštalovať softvérový doplnok <a class="link" href="https://tools.google.com/dlpage/gaoptout" target="_blank">dostupný tu</a>.</p>'
    }
    
    columns: {
		delete_user_id, random_user_id, test_id
	}
	
```

##Postup priadnia typu (odporucam od sucha si precitat dokumentaciu kde pise o tom ako zaregistrovat typ)

# Typy a registre v skratke

Typ je nejaka mala komponenta ktora je sucastou jednej velkej komponenty(da sa to tak predstavit). Kazdy typ riesi nieco ine. Dame si za priklad YourProfile. 
V prvom rade je potrebne zaregistrovat factory do configu. Tento config najdeme v zlozke modulu pod priecinkom config. Je to subor config.neon. 
Ak sme zaregistrovali factory nasledne musime pridat do configu metodu add(). Prvy parameter je factory a druhy je nazov pre dany typ.
Pre ulahcenie prace si mozme vykopirovat typ ako je YourProfile. Zmenime len nazov classy, interface je potrebne upravit podla potreby. 
Kazdy typ ma v sebe metody na export a vymazanie.

# Typy ktore uz existuju

## YourProfile
Tento typ sa stara o osobne udaje ako su meno priezvisko, ulica, ... Tento typ pracuje s tabulkami user a user_info. 
## ShopType 
Export a vymazanie objednavok, zakupenych produktov a faktur.
## GdprType
Exportuje a vymaze data z tabulky gdpr
## ThirdParty
Zobrazi v pdf a jsone ktore trackovacie nastroje vyuziva stranka (ktore zadame v configu v extension)
## ContactType
Export a delete z tabulky kontakt


# Metody v type
 * getTitle() - nazov typu. 
* getTemplatePath() - cesta k sablone ktoru ma tato komponenta
 * prepareDataForRender() - metoda ktora posiela data do sablony
* getData() - tu sa definuju nam vrati vsetky potrebne data pre dany typ. Ako je to v YourProfileControl tak ona vracia vsetky informacie o uzivatelovi. Napriklad ako je meno, adresa, email. 
* delete() - metoda na vymazanie dat, tu je cela logika ak robime delete v type
* anonymize(string) - tato metoda nam zanonymizuje nejaky string napriklad "Peter" -> "P\*\*\*r"



# Cookie bar
## Implementujeme js a css kniznicu a script
```
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
	<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>

	<script>
        window.cookieconsent.initialise({
            container: document.getElementById("content"),

            "palette": {
                "popup": {
                    "background": "#ff3366",
                    "text": "#ffffff"
                },
                "button": {
                    "background": "#ff89b6"
                }
            },
            "position": "bottom-left",
            "type": "opt-in",
            "content": {
                "message": "S cieľom zabezpečiť riadne fungovanie tejto webovej lokality ukladáme niekedy na vašom zariadení malé dátové súbory, tzv. cookie.",
                "link": "Zistiť viac",
                "allow": "Súhlasím",
                "deny": "Zakázať",
				"href": "http://dukra.sk/editor_uploads/files/Pravidl%C3%A1%20ochrany%20osobn%C3%BDch%20%C3%BAdajov.pdf",
                "policy": 'Cookies'
            },
			{if $cookiesStatus == "allow" || $cookiesStatus == "deny"}
			dismissOnTimeout: 100, 
			{/if}
            onInitialise: function(status) {
                $.ajax({
					url: "?do=CookieBar",
					data: {
					    "data": status
					}
				});
            },
            onStatusChange: function(status, chosenBefore) {
                if (status != "dismiss") {
                    $.ajax({
                        url: "?do=CookieBar",
                        data: {
                            "data": status
                        }
                    });
                }
			},

            law: {
                regionalLaw: false,
            },
            location: true,
        });

	</script>
```

Dalej je potrebne vytvorit handleCookieBar ktora prijma parameter status

```
public function handleCookieBar($data)
    {

        $userId = $this->getUser()->getId();

        if (!$userId ) {
            $this->nonLoggedUser($data);
        } else {
            $this->loggedUser($data, $userId);
        }

    }

    private function loggedUser($cookie, $userId)
    {
        $userCookie = $this->database->table($this->prefix . 'gdpr_cookie')->where(['user_id' => $userId])->fetch();

        if (!$userCookie) {
            $data = [
                'user_id' => $userId,
                'status' => $cookie
            ];

            $userCookie = $this->database->table($this->prefix . 'gdpr_cookie')->insert($data);

        } else {
            $userCookie = $this->database->table($this->prefix . 'gdpr_cookie')->where(['user_id' => $userId])->update(['status' => $cookie]);

        }
        $httpResponse = $this->getHttpResponse();
        $httpResponse->setCookie('cookieconsent_status', $cookie, '365 days');

    }

    private function nonLoggedUser($data)
    {
        $httpResponse = $this->getHttpResponse();
        $httpResponse->setCookie('cookieconsent_status', $data, '365 days');

    }
```
Tento kod je este potrebne pridat do createTemplate aby v layout latte boli premenne
```
if ($this->user->isLoggedIn()) {

            $cookie = $this->database->table($this->prefix . 'gdpr_cookie')->where(['user_id' => $this->user->id])
                ->fetch();

            if ($cookie) {
                $httpReponse = $this->getHttpResponse();
                $httpReponse->setCookie('cookieconsent_status', $cookie->status, '365 days');
                $template->cookiesStatus  = $cookie->status;
            } else {
                $httpRequest = $this->getHttpRequest();

                $template->cookiesStatus = $httpRequest->getCookie('cookieconsent_status');
            }


        } else {
            $httpRequest = $this->getHttpRequest();

            $template->cookiesStatus = $httpRequest->getCookie('cookieconsent_status');
        }
```
## Obalit s if podmienkou google analytics a ostatne nastroje
```
		{if $cookiesStatus == "allow"}
        //google analytics script
        {/if?
```

## Pridanie checkboxu 
```
Text: {_'Prečítal(a) som si a súhlasím s prehlásením o ochrane'}<a href="odkaz" target="_blank"> osobných údajov</a>
```
```
Form: $form->addCheckbox('gdpr')
            ->setRequired('Musíte súhlasiť so spracovaním osobných údajov')
            ->setDefaultValue(false);
```

# Problem s css na url gdpr/request a gdpr/landingPage
Je na to pripravena gdpr classa ktoru pretazime v layout.less a upravime uz podla potreby. Zvycajne mi stacil tento kod na upravu css.

```
.gdpr {
	@media (min-width: 768px) {
		float: left;
	}

	.container {
		width: 100%;
	}

	position: relative;
	min-height: 1px;
	padding-left: 15px;
	padding-right: 15px;
}
```

# Do app/presenters/BasePresenter.php umiestniť
```
    public function handleCookieBar($data)
    {

        $userId = $this->getUser()->getId();

        if (!$userId ) {
            $this->nonLoggedUser($data);
        } else {
            $this->loggedUser($data, $userId);
        }

    }

    private function loggedUser($cookie, $userId)
    {
        $userCookie = $this->database->table($this->prefix . 'gdpr_cookie')->where(['user_id' => $userId])->fetch();

        if (!$userCookie) {
            $data = [
                'user_id' => $userId,
                'status' => $cookie
            ];

            $userCookie = $this->database->table($this->prefix . 'gdpr_cookie')->insert($data);

        } else {
            $userCookie = $this->database->table($this->prefix . 'gdpr_cookie')->where(['user_id' => $userId])->update(['status' => $cookie]);

        }
        $httpResponse = $this->getHttpResponse();
        $httpResponse->setCookie('cookieconsent_status', $cookie, '365 days');

    }

    private function nonLoggedUser($data)
    {
        $httpResponse = $this->getHttpResponse();
        $httpResponse->setCookie('cookieconsent_status', $data, '365 days');

    }
```