<?php

require 'bootstrap.php';

class ExportTest extends PHPUnit_Framework_TestCase
{

    /** @var Context */
    private $database;

    /** @var Container */
    private $container;

    /** @var GdprRegister */
    private $gdprRegister;

    /** @var ExportRegister */
    private $exportRegister;

    public function setUp()
    {
        $this->database = Mockery::mock('Nette\Database\Context');
        $this->container = Mockery::mock('Nette\DI\Container');
        $this->gdprRegister = Mockery::mock('Wame\Gdpr\Registers\GdprRegister');
        $this->exportRegister = Mockery::mock('Wame\Gdpr\Export\ExportRegister');
        $this->container->shouldReceive('getParameters');

    }

    public function testSetColumns()
    {
        $export = new \Wame\Gdpr\Export($this->database, $this->container, $this->gdprRegister, $this->exportRegister);
        $setColumns = $export->setColumns(['user_id', 'create_user_id', 'edit_user_id']);
        $this->assertEquals($setColumns->getColumns(), $export->getColumns());
    }
    public function testExportToFile()
    {
        $this->assertEquals(3, 3);
    }

    public function tearDown()
    {
        Mockery::close();
    }
}