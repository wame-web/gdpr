<?php


use Phinx\Migration\AbstractMigration;

class CreateGdprTable extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('wame_gdpr');
        $table->addColumn('user_id', 'integer', ['null' => true])
            ->addColumn('token', 'string')
            ->addColumn('request_date', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('status', 'string', ['default' => 'pending'])
            ->addColumn('download_date', 'datetime', ['null' => true])
            ->addColumn('email', 'string')
            ->create();
    }


    public function down()
    {
        $this->dropTable('wame_gdpr');
    }
}
