<?php


use Phinx\Migration\AbstractMigration;

class CreateGdprTable extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('wame_gdpr');
        $table
            ->addColumn('user_id', 'integer')
            ->addColumn('status', 'string')
            ->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']);
    }


    public function down()
    {
        $this->dropTable('wame_gdpr_cookie');
    }
}
