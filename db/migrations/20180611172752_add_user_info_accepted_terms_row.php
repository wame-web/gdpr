<?php


use Phinx\Migration\AbstractMigration;

class AddUserInfoAcceptedTermsRow extends AbstractMigration
{

    public function change()
    {
        $table = $this->table('wame_user_info');
        $table->addColumn('accepted_terms', 'integer', ['default' => 0])->create();
    }
}
